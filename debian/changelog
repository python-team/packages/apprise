apprise (1.9.2-1) unstable; urgency=medium

  * New upstream version 1.9.2

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Wed, 29 Jan 2025 20:51:18 -0300

apprise (1.9.1-1) unstable; urgency=medium

  * New upstream version 1.9.1
  *  debian/patches/001.fix_manual.patch: Update to new version

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Mon, 23 Dec 2024 20:51:25 -0300

apprise (1.9.0-1) unstable; urgency=medium

  * New upstream version 1.9.0
  * debian/patches/001.fix_manual.patch: Update to new version

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Tue, 10 Sep 2024 21:16:11 -0300

apprise (1.8.1-1) unstable; urgency=medium

  * New upstream version 1.8.1
  * debian/patches/001.fix_manual.patch: Update to new version

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Wed, 31 Jul 2024 21:25:12 -0300

apprise (1.8.0-3) unstable; urgency=medium

  * Bug opened mistakenly (Closes: #1061783)
  * debian/control:
     - Bumped Standards-Version to 4.7.0
     - Modified to dh-sequence-python3
  * debian/copyright: License changed to BSD-2-Clause (Closes: #1073198)
  * debian/rules: build with dh-sequence-python3, unnecessary --with python3

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Sat, 06 Jul 2024 01:03:13 -0300

apprise (1.8.0-2) unstable; urgency=medium

  * debian/control: Fixed Build-Depends for python3-yaml (Closes: #515634)

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Wed, 05 Jun 2024 23:20:33 -0300

apprise (1.8.0-1) unstable; urgency=medium

  * New upstream version 1.8.0

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Thu, 16 May 2024 19:29:30 -0300

apprise (1.7.6-1) unstable; urgency=medium

  * New upstream version 1.7.6
  * debian/rules: Added-test-for-not-execution

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Wed, 24 Apr 2024 19:35:38 -0300

apprise (1.7.5-1) unstable; urgency=medium

  * New upstream version 1.7.5

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Thu, 04 Apr 2024 19:22:40 -0300

apprise (1.7.4-1) unstable; urgency=medium

  * New upstream version 1.7.4
  * debian/patches: Manual header update

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Tue, 12 Mar 2024 20:10:29 -0300

apprise (1.7.2-1) unstable; urgency=medium

  * New upstream version 1.7.2
  * debian/patches: Added new fix

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Thu, 01 Feb 2024 21:21:47 -0300

apprise (1.7.1-1) unstable; urgency=medium

  * New upstream version 1.7.1

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Fri, 29 Dec 2023 23:12:51 -0300

apprise (1.6.0-1) unstable; urgency=medium

  * New upstream version 1.6.0

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Sat, 21 Oct 2023 20:18:00 -0300

apprise (1.5.0-1) unstable; urgency=medium

  * New upstream version 1.5.0
  * debian/rules: Disabled test with error

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Tue, 29 Aug 2023 06:52:13 -0300

apprise (1.4.5-1) unstable; urgency=medium

  * New upstream version 1.4.5
  * Source-only upload to allow testing migration
  * added complete tests directory

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Tue, 11 Jul 2023 20:22:00 -0300

apprise (1.4.0-2) unstable; urgency=medium

  * debian/changelog: removal of white spaces

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Sat, 01 Jul 2023 12:50:22 -0300

apprise (1.4.0-1) unstable; urgency=medium

  * New usptrean version 1.4.0
  * debian/control: - Bumped Standards-Version 4.6.2
  * debian/copyright: updated year to 2023
  * Updadate man upstream

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Sat, 01 Jul 2023 07:18:47 -0300

apprise (1.2.0-3) UNRELEASED; urgency=medium

  * Drop tox from Build-Depends, not used.

 -- Stefano Rivera <stefanor@debian.org>  Sat, 06 May 2023 20:46:55 -0400

apprise (1.2.0-1) unstable; urgency=medium

  * New upstream version 1.2.0
  * debian/control: included build depends to test
  * debian/rules: tests activated
  * debian/tests/control: included test autopkgtests

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Sat, 26 Nov 2022 23:11:23 -0300

apprise (1.1.0-1) unstable; urgency=medium

  * Team upload.
  * d/control:
    + Drop useless, obsolete build-dep python3-cov-core.
    + Add new build-dep pytest-cov.
    + Drop removed build-dep mock, six.
    + Add new build-test-dep python3-dbus.
  * d/gbp.conf: Update default branch name accordingly.
  * d/rules: Enable test but still allow failure.

 -- Boyuan Yang <byang@debian.org>  Sat, 15 Oct 2022 17:58:51 -0400

apprise (1.0.0-1) unstable; urgency=medium

  * New upstream version 1.0.0
  * Upload to unstable
  * d/control: fix repositories address Vcs
  * d/patches: fix acute-accent-in-manual-page

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Wed, 10 Aug 2022 22:55:02 -0300

apprise (0.9.9-1) experimental; urgency=medium

  * Initial release. (Closes: #1014123)

 -- Josenilson Ferreira da Silva <nilsonfsilva@hotmail.com>  Tue, 28 Jun 2022 23:48:22 -0300
